import sqlite3


def create_catter_database(_database_name):  # create the database
    sqlite3.connect(_database_name + '.db')


def create_catter_table(_database_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (ID VARCHAR PRIMARY KEY NOT NULL CHECK (LENGTH(ID) == 4), NAME STRING NOT NULL, WEIGHT FLOAT NOT NULL)')


def add_catter_data(_database_name, _table_name, _id, _name, _weight):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_id, _name, _weight)
    database_cursor.execute(
        f'INSERT INTO {_table_name} VALUES (?, ?, ?)', data)
    database.commit()


def delete_catter_data(_database_name, _table_name, _id):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f"DELETE FROM {_table_name} WHERE ID = '{_id}';")
    database.commit()


def order_catter_by_weight(_database_name, _table_name, _order):
    dir = 'ASC' if _order == 'ascending' else 'DESC'
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    return database_cursor.execute(
        f'SELECT * FROM {_table_name} ORDER BY WEIGHT {dir}')


def get_fattest_catter(_database_name, _table_name):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    return database_cursor.execute(
        f'SELECT ID, NAME, WEIGHT FROM {_table_name} WHERE WEIGHT = (SELECT MAX(WEIGHT) FROM {_table_name})')


def get_thinnest_catter(_database_name, _table_name):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    return database_cursor.execute(
        f'SELECT ID, NAME, WEIGHT FROM {_table_name} WHERE WEIGHT = (SELECT MIN(WEIGHT) FROM {_table_name})')


def get_catter_data(_database_name, _table_name):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    return database_cursor.execute(
        f'SELECT * FROM {_table_name}')


def edit_catter_data(_database_name, _table_name, _id, _parameter, _new_value):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f"UPDATE {_table_name} SET '{_parameter}' = '{_new_value}' WHERE id = '{_id}'")
    database.commit()
