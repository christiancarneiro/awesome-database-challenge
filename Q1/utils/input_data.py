def insert_action():
    print('\n============ SISTEMA DO SR. AMBRÓSIO =================\n')
    print('Insira o número relativo à ação desejada!\n')
    print('1. Registrar novo boi.')
    print('2. Remover boi registrado.')
    print('3. Editar informação de um boi registrado.')
    print('4. Ver todos os bois já registrados.')
    print('5. Descobrir qual o boi mais magro.')
    print('6. Descobrir qual o boi mais pesado.')
    print('7. Sair do sistema.\n')
    return input('Opção: ')


def insert_catter_data():
    print('Maravilha, tu desejas inserir um novo boi? Então...')
    print('insira as seguintes informações acerca do boi a ser registrado: ')
    id = input('id (4 caracteres): ')
    name = input('name: ')
    weight = round(float(input('peso (kg): ')), 2)
    return (id, name, weight)


def insert_catter_id():
    return input('Insira o ID do boi desejado: ')


def insert_catter_changes(id):
    print(f'Você deseja mudar o "ID", o "NOME" ou o "PESO" do boi {id}?')
    parameter = input('R: ')
    new_value = ""
    if parameter == "ID" or "NOME" or "PESO":
        new_value = input(f'Insira o novo {parameter} do boi: ')
        if parameter == "NOME":
            parameter = "NAME"
        elif parameter == "PESO":
            parameter = "WEIGHT"

    return parameter, new_value
