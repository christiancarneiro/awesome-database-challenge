from utils import database_operations, input_data, output_data


def show_catter(_database_name, _table_name):
    print('\nLista de bois já registrados: ')
    try:
        catter_data = database_operations.get_catter_data(
            _database_name, _table_name)
        for item in catter_data.fetchall():
            output_data.output_single_catter(item)
    except Exception as error:
        print('\nA operação falhou: ', error)


def fattest_catter(_database_name, _table_name):
    print('\nDescobrir qual o boi mais pesado')
    try:
        fattest_catter = database_operations.get_fattest_catter(
            _database_name, _table_name)
        output_data.output_single_catter(fattest_catter.fetchone())
    except Exception as error:
        print('\nA operação falhou: ', error)


def thinnest_catter(_database_name, _table_name):
    print('\n Encontrar boi mais magro')
    try:
        thinnest_catter = database_operations.get_thinnest_catter(
            _database_name, _table_name)
        output_data.output_single_catter(thinnest_catter.fetchone())
    except Exception as error:
        print('\nA operação falhou: ', error)


def remove_catter(_database_name, _table_name):
    print('\nRemover boi registrado')
    try:
        id = input_data.insert_catter_id()
        database_operations.delete_catter_data(
            _database_name, _table_name, id)
        print(f'\nboi {id} removido com sucesso!')
    except Exception as error:
        print('\nA operação falhou: ', error)


def add_catter(_database_name, _table_name):
    print('\nRegistrar novo boi')
    try:
        id, name, weight = input_data.insert_catter_data()
        database_operations.add_catter_data(
            _database_name, _table_name, id, name, weight)
        print(f'\nboi {id} registrado com sucesso!')
    except Exception as error:
        print('\nA operação falhou: ', error)


def edit_catter(_database_name, _table_name):
    print('\nEditar informação de um boi registrado')
    try:
        id = input_data.insert_catter_id()
        parameter, new_value = input_data.insert_catter_changes(id)
        database_operations.edit_catter_data(
            _database_name, _table_name, id, parameter, new_value)
        print(
            f'\nO boi {id} teve seu {parameter} alterado para {new_value}!')
    except Exception as error:
        print('\nA operação falhou: ', error)
