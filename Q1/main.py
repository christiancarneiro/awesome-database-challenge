from utils import input_data
from features import actions

# Vou deixando isso aqui, caso no futuro o sr. ambrósio compre uma fazenda nova
database_name = "./Q1/cattle"
table_name = "'cattle data'"


def main():

    action_code = input_data.insert_action()
    if action_code == '1':
        actions.add_catter(database_name, table_name)

    elif action_code == '2':
        actions.remove_catter(database_name, table_name)

    elif action_code == '3':
        actions.edit_catter(database_name, table_name)

    elif action_code == '4':
        actions.show_catter(database_name, table_name)

    elif action_code == '5':
        actions.thinnest_catter(database_name, table_name)

    elif action_code == '6':
        actions.fattest_catter(database_name, table_name)

    elif action_code == '7':
        print('\nSaindo do sistema do Sr. Ambrósio...')
        return

    else:
        print('\nErro na escolha do próximo passo.')

    main()


main()
